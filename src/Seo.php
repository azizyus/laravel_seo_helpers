<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 30.11.2018
 * Time: 13:36
 */

namespace Azizyus\SeoHelpers;


class Seo extends AbstractSeo
{


    public function ogLocale()
    {
        return "en";
    }

    public function ogSiteName()
    {
        return env("APP_NAME");
    }

    public function siteLanguage()
    {
        return "en";
    }


}