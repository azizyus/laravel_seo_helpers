<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 30.11.2018
 * Time: 14:40
 */

namespace Azizyus\SeoHelpers\Factory;


use Azizyus\SeoHelpers\AbstractSeoImplementation;
use Azizyus\SeoHelpers\ISeoData;


class Factory
{
    protected $abstractSeoImplementationClass = AbstractSeoImplementation::class;

    public  function makeInstanceViaImplementedInterface(ISeoData $seo) : String
    {
        $s = new $this->abstractSeoImplementationClass();
        $s->setSeo($seo);
        return $this->instanceToHtml($s);
    }

    public  function instanceToHtml(AbstractSeoImplementation $seo) : String
    {
        return view('SEO::seo')->with(['seo'=>$seo,'currentUrl'=>request()->fullUrl()])->render();
    }
}
