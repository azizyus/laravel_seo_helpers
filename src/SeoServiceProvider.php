<?php


namespace Azizyus\SeoHelpers;


use Illuminate\Support\ServiceProvider;

class SeoServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadViewsFrom(__DIR__."/Html","SEO");
    }

}
