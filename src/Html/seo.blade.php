<meta property="og:locale" content="{{$seo->ogLocale()}}" />
<meta property="og:site_name" content="{{$seo->ogSiteName()}}" />

<meta name="twitter:site" content="{{$seo->twitterSite()}}" />
<meta name="twitter:creator" content="{{$seo->twitterCreator()}}" />
<meta name="twitter:card" content="{{$seo->twitterCard()}}" />

<meta property="og:type" content="{{$seo->ogType()}}" />
<meta property="og:url" content="{{request()->fullUrl()}}" />

<link rel="canonical" href="{{request()->fullUrl()}}" />

<meta property="og:title" content="{{$seo->ogTitle()}}" />
<meta name="twitter:title" content="{{$seo->ogTitle()}}">

<meta property="og:description" content="{!! strip_tags($seo->ogDescription()) !!}" />
<meta name="twitter:description" content="{!! strip_tags($seo->ogDescription()) !!}">
<meta name="description" content="{!! strip_tags($seo->ogDescription()) !!}">

<meta name="content-language" content="{{$seo->getLanguageShortTitle()}}" />
<meta name="keywords" content="{{$seo->seoKeywords()}}" />

<title>{{$seo->ogTitle()}}</title>

