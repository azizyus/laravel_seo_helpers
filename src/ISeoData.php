<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 30.05.2018
 * Time: 18:21
 */

namespace Azizyus\SeoHelpers;




interface ISeoData
{


    public function seoTitle();
    public function seoDescription();
    public function title();
    public function description();
    public function seoImage();
    public function seoKeywords();


}
