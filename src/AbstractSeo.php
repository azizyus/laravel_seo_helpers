<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 30.11.2018
 * Time: 13:25
 */

namespace Azizyus\SeoHelpers;


abstract class AbstractSeo
{

    /**
     * @var ISeoData $seo
     */
    public $seo;


    public function setSeo(ISeoData $seo)
    {
        $this->seo = $this->seo = $seo;
    }


    public function ogLocale()
    {

    }

    public function ogTitle()
    {
        $seoTitle = $this->seo->seoTitle();
        if($seoTitle!="") return $seoTitle;
        else return $this->seo->title();
    }


    public function ogSiteName()
    {

    }

    public function ogDescription()
    {
        $seoDescription = $this->seo->seoDescription();
        if($seoDescription!="") return $seoDescription;
        else return $this->seo->description();
    }

    public function twitterCard()
    {
        return "summary_large_image";
    }


    public function twitterSite()
    {

    }

    public function siteLanguage()
    {
        return "english";
    }

    public function ogImage()
    {
        $image = $this->seo->seoImage();
        return $image;
    }

    public function ogType()
    {
        return "web_site";
    }


    public function twitterCreator()
    {

    }

    public function seoKeywords()
    {
        return $this->seo->seoKeywords();
    }

}
