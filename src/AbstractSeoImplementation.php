<?php


namespace Azizyus\SeoHelpers;


class AbstractSeoImplementation extends Seo
{



    public function ogSiteName()
    {
        return env('APP_NAME');
    }

    public function detailOutput()
    {

        return [

            "seoTitle" => $this->ogTitle(),
            "seoDescription" => $this->ogDescription(),
            "seoImage" => $this->ogImage(),
            "seoLanguage" => $this->ogLocale(),
            "seoOgType" => $this->ogType(),
            "siteName" => $this->ogSiteName(),
            'currentLanguageShortTitle' => $this->getLanguageShortTitle(),
            'keywords' => $this->seoKeywords(),
        ];

    }

    public function getLanguageShortTitle()
    {
        return 'en';
    }

    public function ogLocale()
    {
        return 'en_US';
    }

}
